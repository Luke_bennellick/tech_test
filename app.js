const express = require('express');
const OpenApiValidator = require('express-openapi-validator');
const vehiclesRouter = require('./src/vehicles/vehicles.router.js');
const morgan = require('morgan');

const app = express();

app.use(morgan(process.env.LOG_FORMAT || 'dev'));

app.use(
  OpenApiValidator.middleware({
    apiSpec: './openapi.yaml',
    validateRequests: true,
    validateResponses: true,
  }),
);

app.use((err, req, res, next) => {
  res.status(err.status || 500).json({
    message: err.message,
    errors: err.errors,
  });
});

app.use('/vehicles', vehiclesRouter);

const PORT = process.env.PORT || 8080;

const server = app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

module.exports = server;
