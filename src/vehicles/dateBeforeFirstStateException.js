const dateBeforeFirstStateException = (message) => {
  const error = new Error(message);
  error.name = 'DateBeforeFirstStateException';
  return error;
};

module.exports = { dateBeforeFirstStateException };