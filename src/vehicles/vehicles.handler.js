const { getVehicleAndStateAtTime } = require('./vehicles.service');

const fetchVehicleAndStateForDate = async (req, res) => {
  try {
    const vehicleWithState = await getVehicleAndStateAtTime(
      req.params.vehicle_id,
      req.query.timestamp
    );

    res.status(200).json(
      {
        state: vehicleWithState.stateAtTime,
        vehicle: vehicleWithState.vehicleData
      }
    );
  } catch (err) {
    switch (err.name) {
      case 'vehicleNotFoundException':
        res.status(404).json({ error: err.message });
        break;
      case 'DateBeforeFirstStateException':
        res.status(400).json({ error: err.message });
        break;
      default:
        res.status(500).json({ error: err.message });
    }
  }
};

module.exports = { fetchVehicleAndStateForDate };
