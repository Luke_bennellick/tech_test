const { fetchVehicleWithStates } = require('../database/vehicles/vehicles.client');
const { dateBeforeFirstStateException } = require('./dateBeforeFirstStateException');

const verifyValidTargetDate = (stateLogs, targetDate) => {
  if (targetDate < stateLogs[0].timestamp) {
    throw dateBeforeFirstStateException(
      'The selected date is prior to when the vehicle entered our system'
    );
  }
}

const findStateLogForTargetDate = (stateLogs, timestamp) => {
  const targetDate = new Date(timestamp);

  verifyValidTargetDate(stateLogs, targetDate)

  return stateLogs.find((currentLog, index) => {
    if (index === stateLogs.length - 1) {
      return targetDate >= currentLog.timestamp;
    }

    const nextLog = stateLogs[index + 1];
    const isDateWithinRange = targetDate >= currentLog.timestamp && targetDate < nextLog.timestamp;

    return isDateWithinRange;
  });
}

const getVehicleAndStateAtTime = async (vehicleId, timestamp) => {
  const vehicleDataWithStateLogs = await fetchVehicleWithStates(vehicleId)

  const stateLogAtTime = findStateLogForTargetDate(vehicleDataWithStateLogs.stateLogs, timestamp)

  return {
    stateAtTime: stateLogAtTime.state,
    vehicleData: vehicleDataWithStateLogs.vehicleData
  };
}

module.exports = {
  getVehicleAndStateAtTime,
  dateBeforeFirstStateException
}
