const express = require('express');
const router = express.Router();
const { fetchVehicleAndStateForDate } = require('./vehicles.handler');

router.get('/:vehicle_id/state', fetchVehicleAndStateForDate);

module.exports = router;
