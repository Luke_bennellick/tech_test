const { pool, queryDb } = require('../db');
const { vehicleCache } = require('./cache');
const { vehicleNotFoundException } = require('./vehicleNotFoundException');

const handleMissingVehicleData = (vehicleData, vehicleId) => {
  if (vehicleData.rowCount === 0) {
    throw vehicleNotFoundException(`No vehicle with id ${vehicleId} found`)
  }
}

const fetchStateLogs = async (vehicleId) => {
  const stateLogs = await queryDb(`SELECT * FROM "stateLogs" WHERE "vehicleId"=$1`, [vehicleId]);
  handleMissingVehicleData(stateLogs, vehicleId)

  return stateLogs.rows
}

const fetchVehicle = async vehicleId => {
  const vehicle = await queryDb(`SELECT * FROM vehicles WHERE "id"=$1`, [vehicleId]);
  handleMissingVehicleData(vehicle, vehicleId)

  return vehicle.rows[0]
};

const fetchVehicleWithStates = async (vehicleId) => {
  const vehicleData = vehicleCache.get(vehicleId);

  if (vehicleData === undefined) {
    const [stateLogs, vehicle] = await Promise.all(
      [fetchStateLogs(vehicleId), fetchVehicle(vehicleId)]
    );

    vehicleCache.set(vehicleId, { vehicle, stateLogs }, 60);

    return {
      vehicleData: vehicle,
      stateLogs: stateLogs
    }
  } else {
    return {
      vehicleData: vehicleCache.get(vehicleId).vehicle,
      stateLogs: vehicleCache.get(vehicleId).stateLogs
    }
  }
}

module.exports = { fetchVehicleWithStates, vehicleNotFoundException, pool };
