const vehicleNotFoundException = (message) => {
  const error = new Error(message);
  error.name = 'vehicleNotFoundException';
  return error;
};

module.exports = { vehicleNotFoundException }