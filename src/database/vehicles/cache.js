const NodeCache = require('node-cache');

const vehicleCache = new NodeCache({
  stdTTL: 120,
  checkperiod: 120,
  deleteOnExpire: true,
  maxKeys: 1500
});

module.exports = { vehicleCache }