const { Pool } = require('pg');
const { dbConfig } = require('./dbConfig');

const pool = new Pool(dbConfig);

const queryDb = async (query, queryParams) => {
  const client = await pool.connect();
  try {
    const res = await client.query(query, queryParams);
    return res;
  } finally {
    client.release();
  }
}

module.exports = {
  queryDb,
  pool
}