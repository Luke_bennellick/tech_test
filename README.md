# Note to review:

Thank you for taking the time to review my tech test. I very much hope it meets the expectations for the role. I said I'd get this test back by Tuesday - so down the bottom I've left a few notes around concessions and future improvements. After four years as a Ruby engineer, I have just completed my first year as a Nodejs developer - and as such I would be very grateful for any feedback you have on my code.

# Vehicle State Finding.

This application allows a user to determine the state a vehicle is in on a particular date.

## Setting up for local development

In order to remain system agnostic, local development is most convenient via docker-compose.
Docker-compose will set up all of the required environment variables and supporting services, to allow out of the box
integration testing.

To spin up the services, run the following commands (once you have have docker and compose installed):

`docker-compose up -d --remove-orphans --force-recreate`

If you wish to be able to conveniently view the container logs, omit the `-d` flag.

Once the service is up and running, connect to the container:

`docker exec -it motorway-api /bin/bash`

From here, install the required packages:

`npm install`

Next, run the tests to ensure the service is operating as expected:

`npm test`

And finally, to run the service:

`npm start`

Alternatively, if you wish to skip the above steps and simply use this service for non-development locally, amend the Dockerfile in the section that follows. Comment out the `CMD ["sleep", "infinity"]` line, and uncomment the `# CMD ["node", "app.js"]` line. This is not an ideal solution, but due to time constraints I have omitted separting the Docker files into `production.Dockerfile` and `development.Dockerfile`.

## Interacting with the service.

The endpoints are described in the `openapi.yaml` (which is also used for endpoint validation), however to get you started quickly interacting with the service, requests should be made with the following formatting. The following request will work if you make a request to the API.

GET:
`0.0.0.0:8080/vehicles/1/state?timestamp=2022-09-11T10:23:54%2B00:00`

The vehicle Id must be of type number, and the time stamp must be a valid date-time format. The response codes and expected responses are also documented in the `openapi.yaml` file.

## Design choices and architecture.

This API follows the package-by-feature pattern, which I've had good luck with with smaller APIs. The feature in question pertains to 'vehicles' as a domain object. The example here is a little over-engineered for the use case, but I hope my contrived example has demonstrated an understanding of some ways to scale an API.

The general flow through the system is:

`router (defines routes, calls handler functions) ---->
handler (handles HTTP responses and delegates to service functions to perform logic) ---->
service (performs the bulk of the domain logic - interacts with outside world via client) ---->
client (wrappers around external interactions - i.e databases, third party apis, redis, kafka etc)`

The bulk of request and response validation is performed via the express-openapi-validator middleware.
I've found that the YAML file it uses works well as living documentation, and for ensuring only valid requests and responses are accepted by the API.

In order to improve the ability of the application to handle a larger number of requests, I've added a light (and very basic) caching layer inside of the vehicles client. In addition, the DB is set up to use pooling. Testing with postman, the initial (non-cached) request takes much longer than subsequent requests - the cache significantly improves performance there (from 30ms down to around 7ms on my machine).

## Time concessions and future improvmements.

# Caching

As currently implemented, the caching layer is very basic. With more time, I would prefer to implement an LRU cache (as if this one gets too big - it falls over). In order to further improve performance if the application was part of a load balanced cluster, I believe the optimal solution would be to have a remote cache in Redis.

The caching layer in place has been manually tested, however it would be good to ensure the cache is used (and that stored entries are cleared after one minute) via integration tests.

# Database

The database is flexed fairly well via the router and client tests, however more edgecases would be helpful for building trust in the system. For example:

How does the application handle junk data coming out of the DB? (Typescript interfaces would help here!)

How does the application handle the database being offline/unavailable? One solution to this problem at scale is to put a layer between the service and the DB. Kafka works well here, however this adds complexity of its own.

# Logging & Metrics

The logging is currently barebones. I am logging HTTP responses via the Morgan package, however I would like to add more significant entries (along with performance metrics to discover bottlenecks). For example when cache entries are deleted, when the database is accessed, and when exceptions are raised.

In order to handle logging at scale and to aggregate logs across multiple instances of the service, one solution would be to write a new client for Kafka and send all the logging there (for subsequent consuming via ElasticSearch).

# Separate Docker files

It would be useful to have a production based Dockerfile, which does not include any of the superfluous packages and environment variables required for local development. As much as possible should be removed in order to both reduce attack vectors, and reduce computational overhead.

# Code quality

As always, code quality can be improved. I feel the code is in a fairly good place at least in terms of modularity and knowing what goes where. Still, I believe there are improvements to be made regarding naming and separation of concerns. I am always very happy to receive feedback on improvements during the PR stage. Code is never perfect, and I believe it is important to find a balance between perfect code, and getting features shipped.

# Authentication/Authorisation

I haven't added any authentication or authorisation to the service as it stands. I think the fastest way to secure the service would likely be via JWT with Oauth2 and a backing users table in the DB.

# CI/CD

Nice and easy to setup - the simplest way to get a CI running for the application in my experience would be to use the Gitlab built in CI. I would add an audit stage too, which would fail if any medium level CVEs were detected.
