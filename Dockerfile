FROM node:16
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8080

# Start the application
# CMD ["node", "app.js"]

# Start the application for local testing and development
CMD ["sleep", "infinity"]