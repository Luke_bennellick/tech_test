const request = require('supertest');
const app = require('../../app');
const { pool } = require('../../src/database/vehicles/vehicles.client');
const { vehicleCache } = require('../../src/database/vehicles/cache');

afterAll(async () => {
  app.close();
  await pool.end();
});

afterEach(async () => {
  vehicleCache.flushAll();
});

describe('vehicles/:vehicle_id/state', () => {
  describe('when the vehicle id is valid', () => {
    it('returns the vehicle state and details for a given timestamp', async () => {
      const res = await request(app).get('/vehicles/1/state?timestamp=2022-09-10T10:23:54%2B00:00');

      expect(res.statusCode).toEqual(200);
      expect(res.body.state).toEqual('quoted');
      expect(res.body.vehicle).toEqual(
        { 'id': 1, 'make': 'BMW', 'model': 'X1', 'state': 'quoted' }
      );
    });
  });

  describe('when the vehicle id refers to a vehicle that does not exist', () => {
    it('returns a 404 not found', async () => {
      const res = await request(app).get('/vehicles/4/state?timestamp=2022-09-10T10:23:54%2B00:00');

      expect(res.statusCode).toEqual(404);
      expect(res.body).toEqual({ 'error': 'No vehicle with id 4 found' });
    });
  });

  describe('when the vehicle id is not of number format', () => {
    it('returns a 400 bad request', async () => {
      const res = await request(app).get('/vehicles/abc/state?timestamp=20220529');

      expect(res.statusCode).toEqual(400);
      expect(res.body.message).toEqual(
        'request/query/timestamp must match format "date-time", request/params/vehicle_id must be integer'
      )
    });
  });

  describe('when the state is not of date time format', () => {
    it('returns a 400 bad request', async () => {
      const res = await request(app).get('/vehicles/1/state?timestamp=20220529');

      expect(res.statusCode).toEqual(400);
      expect(res.body.message).toEqual(
        'request/query/timestamp must match format \"date-time\"'
      )
    });
  });

  describe('when the date is prior to the first vehicle state', () => {
    it('returns a 400 bad request', async () => {
      const res = await request(app).get('/vehicles/1/state?timestamp=2010-09-10T10:23:54%2B00:00');

      expect(res.statusCode).toEqual(400);
      expect(res.body).toEqual(
        { 'error': 'The selected date is prior to when the vehicle entered our system' }
      );
    });
  });
});
