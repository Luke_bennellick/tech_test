const { fetchVehicleAndStateForDate } = require('../../src/vehicles/vehicles.handler');
const { getVehicleAndStateAtTime } = require('../../src/vehicles/vehicles.service');

jest.mock('../../src/vehicles/vehicles.service');

describe('fetchVehicleAndStateForDate', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('when vehicle id and timestamp valid', () => {
    it('returns vehicle and state for given date', async () => {
      const mockReq = {
        params: { vehicle_id: 1 },
        query: { timestamp: '2022-09-11T23:11:45+00:00' },
      };
      const mockRes = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const mockVehicleData = { id: 1, make: 'Ford', model: 'Fiesta', };
      const mockStateAtTime = 'sold';

      getVehicleAndStateAtTime.mockResolvedValue({
        vehicleData: mockVehicleData,
        stateAtTime: mockStateAtTime,
      });

      await fetchVehicleAndStateForDate(mockReq, mockRes);

      expect(mockRes.status).toHaveBeenCalledWith(200);
      expect(mockRes.json).toHaveBeenCalledWith({
        vehicle: mockVehicleData,
        state: mockStateAtTime,
      });
    });
  })

  describe('when vehicle is not found', () => {
    it('returns status 404', async () => {
      const mockReq = {
        params: { vehicle_id: 2 },
        query: { timestamp: '2022-09-11T23:11:45+00:00' },
      };
      const mockRes = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };

      const mockError = new Error('Vehicle not found');
      mockError.name = 'vehicleNotFoundException';

      getVehicleAndStateAtTime.mockRejectedValue(mockError);

      await fetchVehicleAndStateForDate(mockReq, mockRes);

      expect(mockRes.status).toHaveBeenCalledWith(404);
      expect(mockRes.json).toHaveBeenCalledWith({ error: mockError.message });
    });
  })

  describe('when provided date is prior to first state', () => {
    it('returns status 400', async () => {
      const mockReq = {
        params: { vehicle_id: 3 },
        query: { timestamp: '2022-09-11T23:11:45+00:00' },
      };
      const mockRes = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };
      const mockError = new Error(
        'The selected date is prior to when the vehicle entered our system'
      );
      mockError.name = 'DateBeforeFirstStateException';

      getVehicleAndStateAtTime.mockRejectedValue(mockError);
      await fetchVehicleAndStateForDate(mockReq, mockRes);

      expect(mockRes.status).toHaveBeenCalledWith(400);
      expect(mockRes.json).toHaveBeenCalledWith({ error: mockError.message });
    });
  })

  describe('when any other unknown exception occurs', () => {
    it('returns status 500', async () => {
      const mockReq = {
        params: { vehicle_id: 3 },
        query: { timestamp: '2022-09-11T23:11:45+00:00' },
      };
      const mockRes = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn(),
      };
      const mockError = new Error('Internal Server Error');
      mockError.name = 'InternalServerError';
      getVehicleAndStateAtTime.mockRejectedValue(mockError);

      await fetchVehicleAndStateForDate(mockReq, mockRes);

      expect(mockRes.status).toHaveBeenCalledWith(500);
      expect(mockRes.json).toHaveBeenCalledWith({ error: mockError.message });
    });
  })
});