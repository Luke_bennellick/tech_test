const { getVehicleAndStateAtTime } = require('../../src/vehicles/vehicles.service');
const { pool } = require('../../src/database/vehicles/vehicles.client');
const { dateBeforeFirstStateException } = require('../../src/vehicles/vehicles.service');
const { vehicleCache } = require('../../src/database/vehicles/cache');

afterAll(async () => {
  await pool.end();
});

afterEach(async () => {
  vehicleCache.flushAll();
});

describe('vehicles.service', () => {
  describe('getVehicleAndStateAtTime for vehicle 1', () => {
    const vehicleData = {
      'id': 1,
      'make': 'BMW',
      'model': 'X1',
      'state': 'quoted'
    }

    it('raises a dateBeforeFirstStateException when date is prior to the first state', async () => {
      await expect(getVehicleAndStateAtTime(1, '2020-09-10 10:23:54+00'))
        .rejects.toThrow(dateBeforeFirstStateException('The selected date is prior to when the vehicle entered our system'));
    });

    it('returns "quoted" state at the specified timestamp', async () => {
      const vehicleState = await getVehicleAndStateAtTime(1, '2022-09-10 10:23:54+00');

      expect(vehicleState).toEqual({
        'stateAtTime': 'quoted',
        vehicleData
      });
    })
  });

  describe('getVehicleAndStateAtTime for vehicle 2', () => {
    const vehicleData = {
      'id': 2,
      'make': 'AUDI',
      'model': 'A4',
      'state': 'selling'
    }

    it('raises a dateBeforeFirstStateException when date is prior to the first state', async () => {
      await expect(getVehicleAndStateAtTime(2, '2019-09-10 10:23:54+00'))
        .rejects.toThrow(dateBeforeFirstStateException('The selected date is prior to when the vehicle entered our system'));
    });

    it('returns "quoted" state at the specified timestamp', async () => {
      const vehicleState = await getVehicleAndStateAtTime(2, '2022-09-10 14:59:01+00');

      expect(vehicleState).toEqual({
        'stateAtTime': 'quoted',
        vehicleData
      });
    })

    it('returns "selling" state at the specified timestamp', async () => {
      const vehicleState = await getVehicleAndStateAtTime(2, '2022-09-11 17:03:17+00');

      expect(vehicleState).toEqual({
        'stateAtTime': 'selling',
        vehicleData
      });
    })
  });

  describe('getVehicleAndStateAtTime for vehicle 3', () => {
    const vehicleData = {
      'id': 3,
      'make': 'VW',
      'model': 'GOLF',
      'state': 'sold'
    }

    it('raises a dateBeforeFirstStateException when date is prior to the first state', async () => {
      await expect(getVehicleAndStateAtTime(3, '1995-09-10 10:23:54+00'))
        .rejects.toThrow(dateBeforeFirstStateException('The selected date is prior to when the vehicle entered our system'));
    });

    it('returns "quoted" state at the specified timestamp', async () => {
      const vehicleState = await getVehicleAndStateAtTime(3, '2022-09-11 23:11:45+00');

      expect(vehicleState).toEqual({
        'stateAtTime': 'quoted',
        vehicleData
      });
    })

    it('returns "selling" state at the specified timestamp', async () => {
      const vehicleState = await getVehicleAndStateAtTime(3, '2022-09-12 12:41:40+00');

      expect(vehicleState).toEqual({
        'stateAtTime': 'selling',
        vehicleData
      });
    })

    it('returns "sold" state at the specified timestamp', async () => {
      const vehicleState = await getVehicleAndStateAtTime(3, '2050-09-12 12:42:41+00');

      expect(vehicleState).toEqual({
        'stateAtTime': 'sold',
        vehicleData
      });
    })
  });
});

