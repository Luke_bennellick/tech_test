const { fetchVehicleWithStates, pool, vehicleNotFoundException } = require('../../src/database/vehicles/vehicles.client');

afterAll(async () => {
  await pool.end();
});

describe('vehicles.client', () => {
  describe('fetchVehicleWithStates', () => {
    describe('when the vehicle does not exist', () => {
      it('raises vehicleNotFoundException', async () => {
        await expect(fetchVehicleWithStates(4))
          .rejects.toThrow(vehicleNotFoundException('No vehicle with id 4 found'));
      })
    })

    describe('when the vehicle exists', () => {
      it('returns the vehicle data with a list of stateLogs', async () => {
        const vehicleWithStates = await fetchVehicleWithStates(3);

        expect(vehicleWithStates).toEqual(
          {
            vehicleData: {
              "id": 3,
              "make": "VW",
              "model": "GOLF",
              "state": "sold"
            },
            stateLogs: [
              {
                vehicleId: 3,
                state: 'quoted',
                timestamp: new Date('2022-09-11T09:11:45.000Z')
              },
              {
                vehicleId: 3,
                state: 'selling',
                timestamp: new Date('2022-09-11T23:21:38.000Z')
              },
              {
                vehicleId: 3,
                state: 'sold',
                timestamp: new Date('2022-09-12T12:41:41.000Z')
              }
            ]
          }
        )
      })
    })
  });
});
